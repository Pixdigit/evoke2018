package main

import (
	"math/rand"
	"sync"
	"time"

	"github.com/pkg/profile"
	"github.com/veandco/go-sdl2/sdl"
)

var buf []byte
var mbl [][2]int
var mbv [][2]int
var mba [][2]int

const (
	//Use these to configure
	screenSizeX int32 = 1920
	screenSizeY int32 = 1080
	ballz             = 14
	mbr               = 25
	threads           = 4

	//Do not adjust this
	bytesPerPixel = 4
)

func min(a, b uint32) uint32 {
	if a < b {
		return a
	} else {
		return b
	}
}

func colorBuf(index int, r, g, b, a uint8) {
	//big endian
	buf[index] = byte(b)
	buf[index+1] = byte(g)
	buf[index+2] = byte(r)
	buf[index+3] = byte(a)
}

func color(r, g, b, a int) uint32 {
	ur := min(uint32(r), 255)
	ug := min(uint32(g), 255)
	ub := min(uint32(b), 255)
	ua := min(uint32(a), 255)

	/*rmask := uint32(0xff000000)
	gmask := uint32(0x00ff0000)
	bmask := uint32(0x0000ff00)
	amask := uint32(0x000000ff)*/
	rmask := uint32(0x00010000)
	gmask := uint32(0x00000100)
	bmask := uint32(0x00000001)
	amask := uint32(0x01000000)
	return uint32(rmask*ur + gmask*ug + bmask*ub + amask*ua)
}

func movement() {
	for mbI := 0; mbI < ballz; mbI++ {
		mbl[mbI][0] += mbv[mbI][0]
		mbl[mbI][1] += mbv[mbI][1]
		mbv[mbI][0] += mba[mbI][0]
		mbv[mbI][1] += mba[mbI][1]
		mba[mbI][0] += rand.Intn(7) - 3
		mba[mbI][1] += rand.Intn(7) - 3
	}

	chkBall := func(indx int) {
		if mbl[indx][0] < 0 || mbl[indx][0] > int(screenSizeX) {
			mbv[indx][0] *= -1
		}
		if mbl[indx][1] < 0 || mbl[indx][1] > int(screenSizeY) {
			mbv[indx][1] *= -1
		}
		maxVel := 40
		if -maxVel > mbv[indx][0] {
			mbv[indx][0] = -maxVel
		}
		if mbv[indx][0] > maxVel {
			mbv[indx][0] = maxVel
		}
		if -maxVel > mbv[indx][1] {
			mbv[indx][1] = -maxVel
		}
		if mbv[indx][1] > maxVel {
			mbv[indx][1] = maxVel
		}
		maxAcc := 5
		if -maxAcc > mba[indx][0] {
			mba[indx][0] = -maxAcc
		}
		if mba[indx][0] > maxAcc {
			mba[indx][0] = maxAcc
		}
		if -maxAcc > mba[indx][1] {
			mba[indx][1] = -maxAcc
		}
		if mba[indx][1] > maxAcc {
			mba[indx][1] = maxAcc
		}
	}
	for mb := range mbl {
		chkBall(mb)
	}

}

func main() {

	defer profile.Start(profile.ProfilePath(".")).Stop()
	rand.Seed(int64(time.Now().UTC().UnixNano()))

	sdl.Init(sdl.INIT_EVERYTHING)


	//Create graphical interfaces
	windowFlags := uint32(sdl.WINDOW_SHOWN) | uint32(sdl.WINDOW_FULLSCREEN_DESKTOP)
	win, _ := sdl.CreateWindow("", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, screenSizeX, screenSizeY, windowFlags)
	win.SetSize(screenSizeX, screenSizeY)

	scr, _ := sdl.CreateRenderer(win, -1, sdl.RENDERER_PRESENTVSYNC)
	dmode, _ := sdl.GetDesktopDisplayMode(0)

	//initialize sizes
	screenRect := &sdl.Rect{0, 0, screenSizeX, screenSizeY}

	tmpText, _ := scr.CreateTexture(sdl.PIXELFORMAT_RGB888, sdl.TEXTUREACCESS_STREAMING, screenSizeX, screenSizeY)

	win.SetFullscreen(sdl.WINDOW_FULLSCREEN)

	scr.Clear()
	scr.Present()
	scr.FillRect(nil)

	oops := time.After(10 * time.Second)

	mbl = make([][2]int, ballz)
	mbv = make([][2]int, ballz)
	mba = make([][2]int, ballz)

	buf = make([]byte, screenSizeX*screenSizeY*4)

	for i := 0; i < ballz; i++ {
		mbl[i] = [2]int{rand.Intn(int(screenSizeX)), rand.Intn(int(screenSizeY))}
		mbv[i] = [2]int{rand.Intn(10), rand.Intn(10)}
		mba[i] = [2]int{0, 0}
	}

	i := 0
	maxByte := len(buf)
	finished := 0
	var wg sync.WaitGroup
	for true {
		for nth := 0; nth < threads; nth++ {
			wg.Add(1)
			go func(nth int, wg *sync.WaitGroup) {
				defer func() { finished++ }()
				for index := nth * maxByte / threads; index < (nth+1)*maxByte/threads; index += bytesPerPixel {
					colorBuf(index, 0, 255, 255, 255)
					x := (index / bytesPerPixel) % (int(screenSizeX))
					y := (index / bytesPerPixel) / (int(screenSizeX))
					val := float64(0)
					for _, mb := range mbl {
						val += 1 / float64((x-mb[0])*(x-mb[0])+(y-mb[1])*(y-mb[1]))
					}
					if val > 1/float64(mbr*mbr*ballz) {
						colorBuf(index, 255, 0, 0, 255)
					}
				}
				wg.Done()
			}(nth, &wg)
		}

		wg.Wait()

		movement()

		tmpText.Update(screenRect, buf, int(screenSizeX)*32/8)
		scr.Copy(tmpText, screenRect, screenRect)
		scr.Present()
		finished = 0
		i++
		select {
		case <-oops:
			return
		default:
		}

	}

	win.SetDisplayMode(&dmode)

}
